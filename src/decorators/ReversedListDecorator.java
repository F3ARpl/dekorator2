package decorators;

import iterators.ReversedListIterator;

import java.util.ArrayList;
import java.util.Iterator;

public class ReversedListDecorator extends AbstractListDecorator {

    public ReversedListDecorator(ArrayList<Integer> arrayList) {
        super(arrayList);
    }

    @Override
    public Iterator<Integer> iterator() {
        return new ReversedListIterator(getArrayList().listIterator(getArrayList().size()));
    }


}
