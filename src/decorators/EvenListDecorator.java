package decorators;

import iterators.EvenListIterator;

import java.util.ArrayList;
import java.util.Iterator;

public class EvenListDecorator extends AbstractListDecorator {

    public EvenListDecorator(ArrayList<Integer> arrayList) {
        super(arrayList);
    }

    @Override
    public Iterator<Integer> iterator() {
        return new EvenListIterator(getArrayList().listIterator());
    }
}
