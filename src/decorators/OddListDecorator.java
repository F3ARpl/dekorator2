package decorators;

import iterators.OddListIterator;

import java.util.ArrayList;
import java.util.Iterator;

public class OddListDecorator extends AbstractListDecorator {

    public OddListDecorator(ArrayList<Integer> arrayList) {
        super(arrayList);
    }

    @Override
    public Iterator<Integer> iterator() {
        return new OddListIterator(getArrayList().listIterator());
    }
}
