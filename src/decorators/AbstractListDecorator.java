package decorators;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class AbstractListDecorator extends ArrayList<Integer> {

    private ArrayList<Integer> arrayList;

    public AbstractListDecorator(ArrayList<Integer> arrayList) {
        super();
        this.arrayList = arrayList;
    }

    @Override
    public abstract Iterator<Integer> iterator();

    public ArrayList<Integer> getArrayList() {
        return arrayList;
    }

}
